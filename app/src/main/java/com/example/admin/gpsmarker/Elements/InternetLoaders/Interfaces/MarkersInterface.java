package com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Admin on 24.05.2016.
 */
public interface MarkersInterface {
    void loadMarker (Response response) throws IOException, JSONException;
}
