package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.MarkersInterface;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 24.05.2016.
 */
public class MarkersLoader extends AsyncTask<String, Void, Response> {

    private MarkersInterface markersInterface;
    private String dateStart;
    private String dateStop;
    private String key;
    private int idPeople;
    private String json_data;

    public MarkersLoader(MarkersInterface markersInterface, String dateStart, String dateStop, String key, int idPeople,String json_data) {
        this.markersInterface = markersInterface;
        this.dateStart = dateStart;
        this.dateStop = dateStop;
        this.key = key;
        this.idPeople = idPeople;
        this.json_data = json_data;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            if (dateStart.compareToIgnoreCase("    -  -  ") == 0)
                dateStart = "";
            if (dateStop.compareToIgnoreCase("    -  -  ") == 0)
                dateStop = "";
            if (json_data.compareToIgnoreCase("") != 0){
                response = client.newCall(ConfigURL.addMarkerCollection(key, json_data)).execute();
            }else {
                response = client.newCall(ConfigURL.getMarcerByWhere(key, idPeople, dateStart, dateStop)).execute();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        try {
             markersInterface.loadMarker(response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
