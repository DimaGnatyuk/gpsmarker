package com.example.admin.gpsmarker.Elements.Fragments.Customs.Array;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.ClassData.RequestItem;
import com.example.admin.gpsmarker.Elements.InternetLoaders.ActionRequest;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.ActionRequestInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.NavigationInterface;
import com.example.admin.gpsmarker.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Response;

/**
 * Created by Admin on 20.05.2016.
 */
public class ArrayRequestAdapter extends ArrayAdapter implements ActionRequestInterface {

    private TextView textViewName;
    private Button buttonAdd;
    private Button buttonDel;
    private List<RequestItem> requestItems;
    private String key;

    private static Button buttonAdd_t;
    private static Button buttonDel_t;
    private static boolean isAdded;
    private NavigationInterface navigationInterface;
    private static int positions;

    public ArrayRequestAdapter(NavigationInterface navigationInterface,Context context, int resource, List<RequestItem> objects, String key) {
        super(context, resource, objects);
        this.requestItems = objects;
        this.key = key;
        this.navigationInterface = navigationInterface;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.custom_array_request, null);
            textViewName = (TextView)v.findViewById(R.id.request_a_tv_name);
            buttonAdd = (Button)v.findViewById(R.id.request_a_btn_add);
            buttonDel = (Button)v.findViewById(R.id.request_a_btn_del);
        }

       RequestItem item = requestItems.get(position);

        if (item != null) {
            textViewName.setText(item.getUser().getName());

            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Added", Toast.LENGTH_SHORT).show();
                    buttonAdd_t = (Button)v;
                    isAdded = true;
                    positions = position;
                    new ActionRequest(getActionRequestInterface(),key,requestItems.get(position).getId(),isAdded).execute();
                }
            });

            buttonDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(),"Delete",Toast.LENGTH_SHORT).show();
                    buttonDel_t = (Button)v;
                    positions = position;
                    isAdded = false;
                    new ActionRequest(getActionRequestInterface(),key,requestItems.get(position).getId(),isAdded).execute();
                }
            });
        }

        return v;
    }

    private ActionRequestInterface getActionRequestInterface (){
        return (ActionRequestInterface)this;
    }

    @Override
    public void actionRequest(Response response) throws IOException, JSONException {
        String s = response.body().string();
        JSONObject object = new JSONObject(s);
        if (object.getInt("error") == 0) {
            if (isAdded) {
                Toast.makeText(getContext(),"Added",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getContext(),"Delete",Toast.LENGTH_SHORT).show();
            }
            navigationInterface.deleteRequestItemFromArray(positions);
        }else{
            Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
        }
    }
}
