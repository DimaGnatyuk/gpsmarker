package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.RequestInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 21.05.2016.
 */
public class SendRequestLoader extends AsyncTask<String, Void, Response> {

    private RequestInterface requestInterface;
    private String key;
    private int idPeople;

    public SendRequestLoader(RequestInterface requestInterface, String key, int idPeople){
        this.requestInterface = requestInterface;
        this.key = key;
        this.idPeople = idPeople;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (data.getInt("error") == 0){
                    if(data.getBoolean("data")){
                        response = client.newCall(ConfigURL.addFriend(key,idPeople)).execute();
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        try {
            requestInterface.sendRequest(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(response);
    }
}
