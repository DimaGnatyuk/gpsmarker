package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.ActionRequestInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 21.05.2016.
 */
public class ActionRequest extends AsyncTask<String, Void, Response> {

    private String key;
    private int idRequest;
    private Boolean idAdded;
    private ActionRequestInterface actionRequestInterface;

    public ActionRequest(ActionRequestInterface actionRequestInterface, String key, int idRequest, Boolean idAdded) {
        this.key = key;
        this.idRequest = idRequest;
        this.idAdded = idAdded;
        this.actionRequestInterface = actionRequestInterface;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (data.getInt("error") == 0){
                    if(data.getBoolean("data")){
                        if (idAdded) {
                            response = client.newCall(ConfigURL.verefiRequestById(key, idRequest)).execute();
                        }else{
                            response = client.newCall(ConfigURL.deleteRequestById(key,idRequest)).execute();
                        }
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        try {
            actionRequestInterface.actionRequest(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(response);
    }
}
