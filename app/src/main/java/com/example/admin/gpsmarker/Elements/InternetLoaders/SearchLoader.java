package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.SearchInterface;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 21.05.2016.
 */
public class SearchLoader extends AsyncTask<String, Void, Response> {

    private String key;
    private String email;
    private SearchInterface searchInterface;

    public SearchLoader (String key, String email, SearchInterface searchInterface){
        this.key = key;
        this.email = email;
        this.searchInterface = searchInterface;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (AutoLoader.test(data)){
                    if(AutoLoader.checkHas(data)){
                        response = client.newCall(ConfigURL.getAllPeopleByEmail(email,key)).execute();
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        try {
            searchInterface.resSearch(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(response);
    }
}
