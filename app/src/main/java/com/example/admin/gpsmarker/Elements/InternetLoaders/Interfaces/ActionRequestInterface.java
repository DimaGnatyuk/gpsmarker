package com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Admin on 21.05.2016.
 */
public interface ActionRequestInterface {
    void actionRequest (Response response) throws IOException, JSONException;
}
