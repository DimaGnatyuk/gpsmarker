package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 02.05.2016.
 */
public class ServiceTrackerLoader extends AsyncTask<String, Void, Response> {

    private String key;
    private double latitude;
    private double longitude;

    public ServiceTrackerLoader(double latitude, double longitude, String key){
        super();
        this.latitude = latitude;
        this.longitude = longitude;
        this.key = key;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (data.getInt("error") == 0){
                    if(data.getBoolean("data")){
                        response = client.newCall(ConfigURL.addMarker(key,longitude,latitude)).execute();
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
