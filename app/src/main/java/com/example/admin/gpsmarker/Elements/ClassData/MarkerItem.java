package com.example.admin.gpsmarker.Elements.ClassData;

/**
 * Created by Admin on 02.05.2016.
 */
public class MarkerItem {
    private int mId;
    private double mLatitude;
    private double mLongitude;
    private String mData;

    public int getId() {
        return mId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double Latitude) {
        this.mLatitude = Latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double Longitude) {
        this.mLongitude = Longitude;
    }

    public void  setData (String data){
        this.mData = data;
    }

    public String getData() {
        return mData;
    }
}
