package com.example.admin.gpsmarker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.RegistrationInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.RegistrationLoader;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;

public class RegistartionActivity extends AppCompatActivity implements RegistrationInterface {

    private EditText name;
    private EditText email;
    private EditText phone;
    private EditText password_1;
    private EditText password_2;
    private Button registration;
    private RegistrationInterface _this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registartion);

        this.name = (EditText)findViewById(R.id.et_name_new);
        this.email = (EditText)findViewById(R.id.et_email_new);
        this.phone = (EditText)findViewById(R.id.et_phone_new);
        this.password_1 = (EditText)findViewById(R.id.et_password_new_1);
        this.password_2 = (EditText)findViewById(R.id.et_password_new_2);
        this.registration = (Button)findViewById(R.id.btn_registration);
        this._this = (RegistrationInterface) this;
        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (password_1.getText().toString().compareToIgnoreCase(password_2.getText().toString()) != 0) {
                        throw new Exception("Incorrect password...");
                    }
                    new RegistrationLoader(_this, name.getText().toString(), email.getText().toString(), phone.getText().toString(), password_1.getText().toString()).execute();

                }catch (Exception error){
                    Toast.makeText(v.getContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void registration(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
                if (AutoLoader.test(jsonRootObject)) {
                        startActivity(new Intent(this,LoginActivity.class));
                    }
                else{
                    startActivity(new Intent(this,RegistartionActivity.class));
                }
        }else{
            startActivity(new Intent(this,RegistartionActivity.class));
        }
    }
}
