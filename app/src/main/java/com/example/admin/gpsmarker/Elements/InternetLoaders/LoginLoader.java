package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.LoginInterface;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 02.05.2016.
 */
public class LoginLoader extends AsyncTask<String, Void, Response> {
    private LoginInterface loginInterface;
    private String login;
    private String password;

    public LoginLoader (LoginInterface loginInterface,String login,String password){
        this.loginInterface = loginInterface;
        this.login = login;
        this.password = password;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.auntification(login,password)).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        try {
            loginInterface.testAuntifiaction(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
