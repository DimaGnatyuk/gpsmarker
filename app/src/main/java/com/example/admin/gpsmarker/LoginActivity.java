package com.example.admin.gpsmarker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.Configs;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.LoginInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.LoginLoader;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements LoginInterface {

    private SharedPreferences sharedPreferences;
    private EditText login = null;
    private EditText password = null;
    private Button logIn = null;
    private Button btn_open_registration = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (EditText) this.findViewById(R.id.et_login);
        password = (EditText) this.findViewById(R.id.et_password);
        logIn = (Button) this.findViewById(R.id.btn_log_in);
        btn_open_registration = (Button) findViewById(R.id.btn_open_registration);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auntification(login.getText().toString(), password.getText().toString());
            }
        });

        btn_open_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), RegistartionActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferences = getSharedPreferences(Configs.APP_PREFERENCES,MODE_PRIVATE);
        //login.setText(sharedPreferences.getString(Configs.LAST_LOGIN,""));
    }

    private void auntification (String login,String password){
        new LoginLoader((LoginInterface) this,login,password).execute();
    }

    @Override
    public void testAuntifiaction(Response response) {
        String s = null;
        try {
            s = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(s);
            if (AutoLoader.test(jsonObject)){
                String has = AutoLoader.parseHasUser(jsonObject);

                sharedPreferences.edit()
                        .putString(Configs.U_KEY, has)
                        .putString(Configs.LAST_LOGIN, login.getText().toString())
                        .commit();
                Intent mainActivity = new Intent(this,MainActivity.class);
                startActivity(mainActivity);
            }else{
                String message = AutoLoader.parseError(jsonObject);
                Toast.makeText(this,message, Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
