package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.RequestsInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 21.05.2016.
 */
public class VerefiRequestLoader extends AsyncTask<String, Void, Response> {

    private String key;
    private RequestsInterface requestsInterface;

    public VerefiRequestLoader( String key,RequestsInterface requestsInterface){
        super();
        this.requestsInterface = requestsInterface;
        this.key = key;
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (data.getInt("error") == 0){
                    if(data.getBoolean("data")){
                        response = client.newCall(ConfigURL.getAllRequests(key)).execute();
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        try {
            requestsInterface.loadRequest(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(response);
    }
}
