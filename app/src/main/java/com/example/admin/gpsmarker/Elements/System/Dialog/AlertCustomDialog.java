package com.example.admin.gpsmarker.Elements.System.Dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.MarkersInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.MarkersLoader;
import com.example.admin.gpsmarker.R;
import com.github.pinball83.maskededittext.MaskedEditText;

/**
 * Created by Admin on 23.05.2016.
 */
public class AlertCustomDialog extends DialogFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static int _day1;
    private static int _month1;
    private static int _birthYear1;
    private static String dateStart = "";

    private static int _day2;
    private static int _month2;
    private static int _birthYear2;
    private static String dateFinish = "";

    private static String key;
    private static int idPeople;

    private static boolean position = true;

    MaskedEditText editText1;
    MaskedEditText editText2;
    Button button;
    MarkersInterface markersInterface;

    public static AlertCustomDialog newInstance(int num,MarkersInterface markersInterface,String key,int idPeople) {
        AlertCustomDialog f = new AlertCustomDialog();
        f.markersInterface = markersInterface;
        f.key = key;
        f.idPeople = idPeople;
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_custom, container, false);
        this.dateStart = "";
        this.dateFinish = "";
        editText1 = (MaskedEditText)v.findViewById(R.id.masked_edit_text1);
        editText2 = (MaskedEditText)v.findViewById(R.id.masked_edit_text2);

        editText1.setMaskIconCallback(new MaskedEditText.MaskIconCallback() {
            @Override
            public void onIconPushed() {
                startDP(true);
            }
        });
        editText2.setMaskIconCallback(new MaskedEditText.MaskIconCallback() {
            @Override
            public void onIconPushed() {
                startDP(false);
            }
        });

        button = (Button)v.findViewById(R.id.data_filters);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateStart = editText1.getText().toString();
                dateFinish = editText2.getText().toString();
                new MarkersLoader(markersInterface,dateStart,dateFinish,key,idPeople,"").execute();
                getDialog().cancel();
            }
        });
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void startDP (boolean p){
        DatePickerDialog dialog = new DatePickerDialog(getContext(), this, 2013, 2, 18);
        this.position = p;
        dialog.show();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (position){
            _birthYear1 = year;
            _month1 = monthOfYear;
            _day1 = dayOfMonth;

            String day = _day1 <10? "0"+_day1: _day1+"";
            String month = _month1 <10? "0"+_month1: _month1+"";
            //this.dateStart = _birthYear1 + "-" + month + "-" + day;
            this.editText1.setText(_birthYear1 + "-" + month + "-" + day);
        }else{
            _birthYear2 = year;
            _month2 = monthOfYear;
            _day2 = dayOfMonth;

            String day = _day2 <10? "0"+_day2: _day2+"";
            String month = _month2 <10? "0"+_month2: _month2+"";
            //this.dateFinish = _birthYear2 + "-" + month + "-" + day;
            this.editText2.setText(_birthYear2 + "-" + month + "-" + day);
        }

    }
}
