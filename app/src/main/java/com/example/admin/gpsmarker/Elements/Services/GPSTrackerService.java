package com.example.admin.gpsmarker.Elements.Services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.ClassData.MarkerItem;
import com.example.admin.gpsmarker.Elements.Configs;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.MarkersInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.MarkersLoader;
import com.example.admin.gpsmarker.Elements.InternetLoaders.ServiceTrackerLoader;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import okhttp3.Response;

/**
 * Created by Admin on 02.05.2016.
 */
public class GPSTrackerService extends Service implements MarkersInterface {
    private static final String KEY_COLLECTION_OBJECT_MARKER = "KEY_COLLECTION_OBJECT_MARKER";
    public LocationManager locationManager;
    private SharedPreferences preferences;
    private String key = "";

    static Collection<MarkerItem> markerItemCollection = null;

    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        preferences = getSharedPreferences(Configs.APP_PREFERENCES, Context.MODE_PRIVATE);
        Log.d("location", "onCreate");
        String temp = preferences.getString(KEY_COLLECTION_OBJECT_MARKER, "");
        if(temp.compareToIgnoreCase("") != 0) {
            Gson gson = new Gson();
            this.markerItemCollection = gson.fromJson(temp, Collection.class);
        }else{
            this.markerItemCollection = new HashSet<>();
        }

    }

    private void sendCollectionMarket (){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()){
            Gson gson = new Gson();
            String s = gson.toJson(this.markerItemCollection);
            if (s.compareToIgnoreCase("[]") != 0) {
                new MarkersLoader((MarkersInterface) this, "", "", key, 0, gson.toJson(this.markerItemCollection)).execute();
            }
        }
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo==null || !networkInfo.isConnectedOrConnecting()){
                    throw new Exception();
                }else {
                    new ServiceTrackerLoader(location.getLatitude(), location.getLongitude(), key).execute();
                    sendCollectionMarket();
                }
            }catch (Exception e){
                MarkerItem markerItem = new MarkerItem();
                markerItem.setLatitude(location.getLatitude());
                markerItem.setLongitude(location.getLongitude());
                Date date = new Date();
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                markerItem.setData(df.format(date));
                markerItemCollection.add(markerItem);

                Gson gson = new Gson();
                String json = gson.toJson(markerItemCollection);
                preferences.edit().putString(KEY_COLLECTION_OBJECT_MARKER, json).commit();
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("location", "onStartCommand");
        this.key = preferences.getString(Configs.U_KEY, "");
        if (this.key.compareToIgnoreCase("") == 0){
            this.stopSelf();
        }
        gpsTracker();
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("location", "onBind");
        return null;
    }

    private void gpsTracker() {
        Log.d("location", "gpsTracker");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                10, 1, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 10, 1,
                locationListener);
    }


    @Override
    public void onDestroy() {
        Log.d("location", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void loadMarker(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                preferences.edit().putString(KEY_COLLECTION_OBJECT_MARKER, "").commit();
                this.markerItemCollection.clear();
            }
        }else{
            Toast.makeText(this,"Internet Error",Toast.LENGTH_SHORT).show();
        }
    }
}
