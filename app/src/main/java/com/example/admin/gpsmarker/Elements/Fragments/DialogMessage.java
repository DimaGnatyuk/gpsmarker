package com.example.admin.gpsmarker.Elements.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.example.admin.gpsmarker.Elements.Configs;
import com.example.admin.gpsmarker.OptionsActivity;
import com.example.admin.gpsmarker.R;

/**
 * Created by Admin on 22.05.2016.
 */
public class DialogMessage {
    public static void shewDialog(final Context context, int idTitle, int idMessage, final int toDo){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(idMessage)
                .setTitle(idTitle);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (toDo == Configs.RUN_OPTION_ACTIVITY) {
                    context.startActivity(new Intent(context,OptionsActivity.class));
                }else if(toDo == Configs.NOT_RUN){
                    dialog.cancel();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
