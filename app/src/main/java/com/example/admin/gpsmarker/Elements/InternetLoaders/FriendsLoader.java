package com.example.admin.gpsmarker.Elements.InternetLoaders;

import android.os.AsyncTask;

import com.example.admin.gpsmarker.Elements.ConfigURL;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.FriendsInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Admin on 02.05.2016.
 */
public class FriendsLoader extends AsyncTask<String, Void, Response> {

    private FriendsInterface friendsInterface;
    private String key;
    public FriendsLoader(FriendsInterface friendsInterface, String key){
        super();
        this.friendsInterface = friendsInterface;
        this.key = key;
    }


    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(ConfigURL.checkToken(key)).execute();
            String jsonData = response.body().string();
            JSONObject data = null;
            try {
                data = new JSONObject(jsonData);
                if (data.getInt("error") == 0){
                    if(data.getBoolean("data")){
                        response = client.newCall(ConfigURL.getAllFriends(key)).execute();
                    }else{
                        response = null;
                    }
                }else{
                    response = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        try {
            friendsInterface.getFriends(response);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}