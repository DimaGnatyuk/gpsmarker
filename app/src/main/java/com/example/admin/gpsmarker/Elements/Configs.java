package com.example.admin.gpsmarker.Elements;

/**
 * Created by Admin on 02.05.2016.
 */
public class Configs {
    public static final String APP_PREFERENCES = "GPS_MARKER";

    public static final String U_KEY = "token_has";
    public static final String USER_PI = "user_id";

    public static final String PROGRAM_HAS_FRIENDS = "PROGRAM_HAS_FRIENDS";
    public static final String PROGRAM_HAS_REQUEST = "PROGRAM_HAS_FRIENDS";

    public static final String LAST_LOGIN = "last_this_client";
    public static final String GPS_SERVICE = "on_start_gps_service";

    public static final int NOTIFICATION = 1500;

    public static final int NOT_RUN = 0;
    public static final int RUN_OPTION_ACTIVITY = 100;
}
