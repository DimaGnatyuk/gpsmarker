package com.example.admin.gpsmarker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.ClassData.RequestItem;
import com.example.admin.gpsmarker.Elements.ClassData.User;
import com.example.admin.gpsmarker.Elements.Configs;
import com.example.admin.gpsmarker.Elements.Fragments.Customs.Array.ArrayRequestAdapter;
import com.example.admin.gpsmarker.Elements.Fragments.Customs.Array.ArraySearchAdapter;
import com.example.admin.gpsmarker.Elements.Fragments.DialogMessage;
import com.example.admin.gpsmarker.Elements.InternetLoaders.FriendsLoader;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.FriendsInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.NavigationInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.RequestsInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.SearchInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.SearchLoader;
import com.example.admin.gpsmarker.Elements.InternetLoaders.VerefiRequestLoader;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;
import com.example.admin.gpsmarker.Elements.Services.GPSTrackerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements FriendsInterface, SearchInterface, RequestsInterface, NavigationInterface {

    private SharedPreferences sharedPreferences;

    private ListView listViewPeople;
    private EditText emailET;
    private Button searchBTN;

    private String key = "";

    private List<User> friends = new ArrayList<>();
    private List<User> searchPeoples = new ArrayList<>();
    private List<RequestItem> requests = new ArrayList<>();

    private SearchInterface searchInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        searchInterface = (SearchInterface)this;
        sharedPreferences = getSharedPreferences(Configs.APP_PREFERENCES, Context.MODE_PRIVATE);

        key = sharedPreferences.getString(Configs.U_KEY, "");

        emailET = (EditText)findViewById(R.id.ed_a_main_email);
        searchBTN = (Button)findViewById(R.id.btn_f_main_search);
        listViewPeople = (ListView) findViewById(R.id.listPeoples);


        searchBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SearchLoader(key,emailET.getText().toString(),searchInterface).execute();
            }
        });


        listViewPeople.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle bundle = new Bundle();
                bundle.putInt(Configs.USER_PI, friends.get(position).getId());
                Intent intent = new Intent(view.getContext(), FriendActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });




        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.calendar_filter, null))
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        Dialog dialog = builder.create();
        dialog.show();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.form_friends:
                new FriendsLoader((FriendsInterface)this,key).execute();
                return true;
            case R.id.form_request:
                new VerefiRequestLoader(key,(RequestsInterface)this).execute();
                return true;
            case R.id.form_option:
                    startActivity(new Intent(this,OptionsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startFirendsArray(){
        ArrayList<String> list = new ArrayList<String>();
        for (User friend:friends) {
            list.add(friend.getName());
        }

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listViewPeople.setAdapter(adapter);
    }

    private void srartRequestArray(){
        ArrayRequestAdapter arrayRequestAdapter = new ArrayRequestAdapter((NavigationInterface)this,this,R.layout.custom_array_request,requests,key);
        listViewPeople.setAdapter(arrayRequestAdapter);
    }

    private void srartSearchArray(){
        Toast.makeText(this, emailET.getText(), Toast.LENGTH_SHORT).show();

        ArraySearchAdapter arraySearchAdapter = new ArraySearchAdapter(this,R.layout.custom_array_search,searchPeoples,key);
        listViewPeople.setAdapter(arraySearchAdapter);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (key.compareToIgnoreCase("") != 0) {
            if(sharedPreferences.getBoolean(Configs.GPS_SERVICE,true)){
            //if(false){
                Bundle bundle = new Bundle();
                bundle.putString(Configs.U_KEY, key);
                Intent intent = new Intent(this, GPSTrackerService.class);
                intent.putExtras(bundle);
                startService(intent);
                //if()

                new FriendsLoader((FriendsInterface)this,key).execute();
            }else{
                DialogMessage.shewDialog(this, R.string.error, R.string.service_error, Configs.RUN_OPTION_ACTIVITY);
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void getFriends(Response response) throws JSONException, IOException {
        if(response != null) {
            try {
                String jsonData = response.body().string();
                JSONObject jsonRootObject = new JSONObject(jsonData);
                if (AutoLoader.test(jsonRootObject)) {
                    friends = AutoLoader.parseFriends(jsonRootObject);
                    startFirendsArray();
                }
            }catch (Exception error){
                Toast.makeText(this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void resSearch(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                searchPeoples = AutoLoader.parsePeoples(jsonRootObject);
                srartSearchArray();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void loadRequest(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                requests = AutoLoader.parseRequests(jsonRootObject);
                srartRequestArray();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void deleteRequestItemFromArray(int posotion) {
        requests.remove(posotion);
        srartRequestArray();
    }
}
