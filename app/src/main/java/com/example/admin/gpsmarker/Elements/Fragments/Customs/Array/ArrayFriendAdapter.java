package com.example.admin.gpsmarker.Elements.Fragments.Customs.Array;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.admin.gpsmarker.R;

import java.util.List;

/**
 * Created by Admin on 21.05.2016.
 */
public class ArrayFriendAdapter extends ArrayAdapter {
    public ArrayFriendAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ArrayFriendAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ArrayFriendAdapter(Context context, int resource, Object[] objects) {
        super(context, resource, objects);
    }

    public ArrayFriendAdapter(Context context, int resource, int textViewResourceId, Object[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public ArrayFriendAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    public ArrayFriendAdapter(Context context, int resource, int textViewResourceId, List objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.custom_array_friend, null);
            /*textViewName = (TextView)convertView.findViewById(R.id.search_a_tv_name);
            buttonAdd = (Button)convertView.findViewById(R.id.search_a_btn_add);*/
        }

       /* item = users[position];

        if (item != null) {
            textViewName.setText(item.getName());
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("", item.getEmail());
                }
            });
        }*/

        return v;
    }
}
