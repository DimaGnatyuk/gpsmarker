package com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces;

import org.json.JSONException;
import java.io.IOException;
import okhttp3.Response;

/**
 * Created by Admin on 02.05.2016.
 */
public interface FriendsInterface {
    void getFriends (Response response) throws JSONException, IOException;
}
