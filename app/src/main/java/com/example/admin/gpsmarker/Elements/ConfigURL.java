package com.example.admin.gpsmarker.Elements;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Admin on 21.05.2016.
 */
public class ConfigURL {
    private static String BASE_ADRES = "http://android-projects.esy.es/";
    private static String API_URL = "api/";
    public static String BASE_URL = BASE_ADRES+API_URL;

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static Request auntification (String email, String password){
        RequestBody formBody = new FormBody.Builder()
                .add("login", email)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "auntification")
                .post(formBody)
                .build();
        return request;
    }

    public static Request checkToken (String key){
        Request isValid = new Request.Builder()
                .url(BASE_URL + "isValid?key=" + key)
                .build();
        return isValid;
    }

    public static Request registration (String name,String email,String phone,String password){
        RequestBody formBody = new FormBody.Builder()
                .add("name", name)
                .add("email", email)
                .add("phone", phone)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "addPeople")
                .post(formBody)
                .build();
        return request;
    }

    public static Request getAllFriends (String key){
        Request request = new Request.Builder()
                .url(BASE_URL + "getAllFriends?key=" + key)
                .build();
        return request;
    }

    public static Request getFriendById (String key, int idFriend){
        Request request = new Request.Builder()
                .url(BASE_URL + "getFriend/"+idFriend+"?key=" + key)
                .build();
        return request;
    }

    public static Request addMarker (String key,double longitude,double latitude){
        Request request = new Request.Builder()
                .url(BASE_URL + "addMarker/"+String.valueOf(latitude)+"/"+String.valueOf(longitude)+"?key=" + key)
                .build();
        return request;
    }

    public static Request addMarkerCollection (String key, String json_data){
        FormBody.Builder builder = new FormBody.Builder();
        builder.add("json_data",json_data);
        RequestBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(BASE_URL + "addCollectionMarkers"+"?key=" + key)
                .post(formBody)
                .build();
        return request;
    }

    public static Request addFriend (String key, int idPeople){
        Request request = new Request.Builder()
                .url(BASE_URL + "addFriend/"+idPeople+"?key=" + key)
                .build();
        return request;
    }

    public static Request getAllMarkersByIdFriend (String key,int idFriend){
        return null;
    }

    public static Request getAllPeopleByEmail (String email, String key){
        Request request = new Request.Builder()
                .url(BASE_URL + "getPeoplesByEmail/"+email+"?key=" + key)
                .build();
        return request;
    }

    public static Request getAllRequests (String key){
        Request request = new Request.Builder()
                .url(BASE_URL + "getRequest/"+"?key=" + key)
                .build();
        return request;
    }

    public static Request sendRequests (String key,int idPeople){
        return null;
    }

    public static Request verefiRequestById (String key,int idRequest){
        Request request = new Request.Builder()
                .url(BASE_URL + "verefiFriend/"+idRequest+"/1?key=" + key)
                .build();
        return request;
    }

    public static Request deleteFriend (String key, int idFriend){
        Request request = new Request.Builder()
                .url(BASE_URL + "deleteFriend/"+idFriend+"?key=" + key)
                .build();
        return request;
    }

    public static Request deleteRequestById (String key,int idRequest){
        Request request = new Request.Builder()
                .url(BASE_URL + "deleteRequest/" + idRequest + "?key=" + key)
                .build();
        return request;
    }

    public static Request getMarcerByWhere (String key,int idFriend,String dataStart, String dataFinish){

        FormBody.Builder builder = new FormBody.Builder();
        if (dataStart.compareToIgnoreCase("") != 0){
            builder.add("dataStart", dataStart);
        }

        if (dataFinish.compareToIgnoreCase("") != 0){
            builder.add("dataFinish", dataFinish);
        }
        RequestBody formBody = builder.build();

        Request request = new Request.Builder()
                .url(BASE_URL + "getMarkers/"+idFriend+"?key=" + key)
                .post(formBody)
                .build();
        return request;
    }


}
