package com.example.admin.gpsmarker.Elements.ClassData;

import java.util.List;

/**
 * Created by Admin on 02.05.2016.
 */
public class User {
    private int mId;
    private String mName;
    private String mPhone;
    private String mEmail;

    private List<MarkerItem> markerItemList;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public List<MarkerItem> getMarkerItemList() {
        return markerItemList;
    }

    public void setMarkerItemList(List<MarkerItem> markerItemList) {
        this.markerItemList = markerItemList;
    }
}
