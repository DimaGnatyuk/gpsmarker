package com.example.admin.gpsmarker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.ClassData.MarkerItem;
import com.example.admin.gpsmarker.Elements.ClassData.User;
import com.example.admin.gpsmarker.Elements.Configs;
import com.example.admin.gpsmarker.Elements.InternetLoaders.DeleteFriendLoader;
import com.example.admin.gpsmarker.Elements.InternetLoaders.FriendLoader;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.DeleteFriendInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.FriendInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.MarkersInterface;
import com.example.admin.gpsmarker.Elements.Parsers.AutoLoader;
import com.example.admin.gpsmarker.Elements.System.Dialog.AlertCustomDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import okhttp3.Response;

public class FriendActivity extends AppCompatActivity implements OnMapReadyCallback, FriendInterface, DeleteFriendInterface, MarkersInterface {

    private GoogleMap mMap;
    private SharedPreferences sharedPreferences = null;
    private Collection<MarkerItem> markersList = new HashSet<MarkerItem>();
    private TextView textViewName;
    private TextView textViewPhone;
    private TextView textViewEmail;
    private User friend;
    private Marker marker;
    private String key = "";

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);


        nvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                return selectDrawerItem(item);
            }
        });

        int id_people = getIntent().getIntExtra(Configs.USER_PI, -1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nvView);
        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header);
        textViewName = (TextView)headerLayout.findViewById(R.id.people_name_title);
        textViewPhone = (TextView)headerLayout.findViewById(R.id.people_phone_title);
        textViewEmail = (TextView)headerLayout.findViewById(R.id.people_email_title);

        sharedPreferences = getSharedPreferences(Configs.APP_PREFERENCES, Context.MODE_PRIVATE);
        key = sharedPreferences.getString(Configs.U_KEY, "");

        new FriendLoader((FriendInterface) this,key,id_people).execute();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.common_open_on_phone,  R.string.common_open_on_phone);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.manu_friend, menu);
        return true;
    }



    public boolean selectDrawerItem(MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.all_marker:
                Toast.makeText(this,"Filter - All",Toast.LENGTH_SHORT).show();
                break;
            case R.id.today_marker:
                Toast.makeText(this,"Filter - today_marker",Toast.LENGTH_SHORT).show();
                break;
            case R.id.yesterday_marker:
                Toast.makeText(this,"Filter - yesterday_marker",Toast.LENGTH_SHORT).show();
                break;
            case R.id.filter_marker:
                Toast.makeText(this,"Filter - filter_marker",Toast.LENGTH_SHORT).show();
                startFilter();
                break;
        }

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);

        // Close the navigation drawer
        mDrawer.closeDrawers();
        return true;
    }

    void startFilter (){
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        DialogFragment newFragment = AlertCustomDialog.newInstance(1234,(MarkersInterface)this,key,friend.getId());
        newFragment.show(ft,"2323");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete_friend:
                new DeleteFriendLoader((DeleteFriendInterface)this,this.key,this.friend.getId()).execute();
                Toast.makeText(this,this.friend.getName()+" - Deleted",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
    }

    private void updateMarkers (){
        mMap.clear();

        MarkerItem markerItem = null;
        for(MarkerItem marker : markersList) {
            LatLng sydney = new LatLng(marker.getLatitude(), marker.getLongitude());
            float color = BitmapDescriptorFactory.HUE_AZURE;

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String d1 = DateFormat.format("yyyy-MM-dd", new java.util.Date()).toString();
            Date date1 = new Date();
            Date date2 = new Date();
            try {
                date1 = format.parse(marker.getData());
                date2 = format.parse(d1);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (date1.equals(date2)){
                color = BitmapDescriptorFactory.HUE_CYAN;
            }
            this.marker = this.mMap.addMarker(new MarkerOptions().position(sydney).title(marker.getData()).snippet(friend.getName()).icon(BitmapDescriptorFactory.defaultMarker(color)));
            this.mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            markerItem = marker;
        }
        if (markerItem != null){
            LatLng MOUNTAIN_VIEW = new LatLng(markerItem.getLatitude(),markerItem.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(MOUNTAIN_VIEW)
                    .zoom(12)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            this.marker.setIcon(BitmapDescriptorFactory.defaultMarker());
            this.marker.showInfoWindow();
        }
    }

    @Override
    public void getInfo(Response response) throws JSONException, IOException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                this.friend = AutoLoader.getFriend(jsonRootObject);
                this.setTitle(friend.getName());
                this.textViewName.setText("Name - "+friend.getName());
                this.textViewEmail.setText("Email - "+friend.getEmail());
                this.textViewPhone.setText("Phone - "+friend.getPhone());
                this.markersList = friend.getMarkerItemList();
                updateMarkers ();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void deleteFriend(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                startActivity(new Intent(this,MainActivity.class));
            }else{
                Toast.makeText(this,"Server Error",Toast.LENGTH_SHORT).show();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    @Override
    public void loadMarker(Response response) throws IOException, JSONException {
        if(response != null) {
            String jsonData = response.body().string();
            JSONObject jsonRootObject = new JSONObject(jsonData);
            if (AutoLoader.test(jsonRootObject)) {
                List<MarkerItem> markerItems = AutoLoader.parseMarkers(jsonRootObject);
                this.markersList = markerItems;
                updateMarkers ();
            }
        }else{
            startActivity(new Intent(this,LoginActivity.class));
        }
    }
}
