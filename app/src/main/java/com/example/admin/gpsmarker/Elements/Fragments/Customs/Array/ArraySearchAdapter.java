package com.example.admin.gpsmarker.Elements.Fragments.Customs.Array;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.gpsmarker.Elements.ClassData.User;
import com.example.admin.gpsmarker.Elements.InternetLoaders.Interfaces.RequestInterface;
import com.example.admin.gpsmarker.Elements.InternetLoaders.SendRequestLoader;
import com.example.admin.gpsmarker.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Response;

/**
 * Created by Admin on 20.05.2016.
 */
public class ArraySearchAdapter extends ArrayAdapter implements RequestInterface {

    private String key;
    private TextView textViewName;
    private Button buttonAdd;
    private List<User> users = null;
    private static Button buttonAdd_t;

    public ArraySearchAdapter(Context context, int resource, List<User> objects, String key) {
        super(context, resource, objects);
        users = objects;
        this.key = key;
    }

    public RequestInterface getRequestInterface (){
        return (RequestInterface)this;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.custom_array_search, null);
            textViewName = (TextView)v.findViewById(R.id.search_a_tv_name);
            buttonAdd = (Button)v.findViewById(R.id.search_a_btn_add);
        }

        User item = users.get(position);

        if (item != null) {
            textViewName.setText(item.getName());
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonAdd_t = (Button) v;
                    Toast.makeText(v.getContext(), users.get(position).getName(), Toast.LENGTH_SHORT).show();
                    new SendRequestLoader(getRequestInterface(), key, users.get(position).getId()).execute();
                }
            });
        }
        return v;
    }

    @Override
    public void sendRequest(Response response) throws IOException, JSONException {
        String s = response.body().string();
        JSONObject object = new JSONObject(s);
        if (object.getInt("error") == 0) {
            buttonAdd_t.setText("Заявку відправлено");
            buttonAdd_t.setEnabled(false);
        }else{
            Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            buttonAdd_t.setText("Помилка..");
            buttonAdd_t.setEnabled(false);
        }
    }

}
