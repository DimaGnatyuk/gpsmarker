package com.example.admin.gpsmarker.Elements.Parsers;

import android.util.Log;

import com.example.admin.gpsmarker.Elements.ClassData.MarkerItem;
import com.example.admin.gpsmarker.Elements.ClassData.RequestItem;
import com.example.admin.gpsmarker.Elements.ClassData.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 21.05.2016.
 */
public class AutoLoader {

    public static boolean test (JSONObject object) throws JSONException {
        return  object.getInt("error") == 0 ? true : false;
    }

    public static boolean checkHas (JSONObject object) throws JSONException {
        return  object.getBoolean("data");
    }

    public static List<User> parsePeoples (JSONObject jsonRootObject) throws JSONException {
        List<User> people =  new ArrayList<>();
        JSONArray arrayPeople = jsonRootObject.getJSONArray("data");
        for(int i=0;i<arrayPeople.length();i++)
        {
            JSONObject friend = arrayPeople.getJSONObject(i);
            JSONObject peopleInfo = friend.getJSONObject("People");
            User p = new User();
            p.setId(peopleInfo.getInt("id"));
            p.setName(peopleInfo.getString("name"));
            p.setEmail(peopleInfo.getString("email"));
            p.setPhone(peopleInfo.getString("phone"));
            people.add(p);
        }
        return people;
    }

    public static List<User> parseFriends (JSONObject jsonRootObject) throws JSONException {
        List<User> users =  new ArrayList<>();
        JSONArray arrayfriends = jsonRootObject.getJSONArray("data");
        for(int i=0;i<arrayfriends.length();i++)
        {
            JSONObject friend = arrayfriends.getJSONObject(i);
            JSONObject peopleInfo = friend.getJSONObject("People");
            User people = new User();
            people.setId(peopleInfo.getInt("id"));
            people.setName(peopleInfo.getString("name"));
            people.setEmail(peopleInfo.getString("email"));
            people.setPhone(peopleInfo.getString("phone"));
            users.add(people);
        }
        return users;
    }

    public static User parseUser (JSONObject jsonObject) throws JSONException {
        JSONObject data = jsonObject.getJSONObject("data");
        JSONObject user = data.getJSONObject("People");

        User client = new User();
        client.setId(user.getInt("id"));
        client.setName(user.getString("name"));
        return client;
    }

    public static User getFriend (JSONObject jsonRootObject) throws JSONException {
        List<MarkerItem> markerItems = new ArrayList<>();
        User user = new User();
        JSONObject object =  jsonRootObject.getJSONObject("data");
        Log.d("JSON", object.toString());
        JSONObject peopleInfo = object.getJSONObject("People");

        user.setId(peopleInfo.getInt("id"));
        user.setName(peopleInfo.getString("name"));
        user.setPhone(peopleInfo.getString("phone"));
        user.setEmail(peopleInfo.getString("email"));

        JSONArray markers = object.getJSONArray("Marker");
        for (int i=0;i<markers.length();i++){
            JSONObject objectMarker = markers.getJSONObject(i);
            MarkerItem marker = new MarkerItem();
            marker.setData(objectMarker.getString("data"));
            marker.setLongitude(objectMarker.getDouble("longitude"));
            marker.setLatitude(objectMarker.getDouble("latitude"));

            markerItems.add(marker);
        }
        user.setMarkerItemList(markerItems);
        return user;
    }

    public static String parseHasUser(JSONObject jsonObject) throws JSONException {
        return jsonObject.getJSONObject("data").getJSONObject("People").getString("has_md");
    }

    public static String parseError(JSONObject jsonObject) throws JSONException {
        return jsonObject.getString("data");
    }

    public static List<MarkerItem> parseMarkers (JSONObject jsonRootObject) throws JSONException {
        List<MarkerItem> markerItems = new ArrayList<>();
        JSONArray jsonArray =  jsonRootObject.getJSONArray("data");
        for (int i=0;i<jsonArray.length();i++){
            JSONObject markers_o = jsonArray.getJSONObject(i);
            JSONObject markers = markers_o.getJSONObject("Marker");
            MarkerItem marker = new MarkerItem();
            marker.setData(markers.getString("data"));
            marker.setLongitude(markers.getDouble("longitude"));
            marker.setLatitude(markers.getDouble("latitude"));

            markerItems.add(marker);
        }

        return markerItems;
    }

    public static List<RequestItem> parseRequests (JSONObject jsonRootObject) throws JSONException {
        List<RequestItem> requestItems = new ArrayList<>();

        JSONArray arrayfriends = jsonRootObject.getJSONArray("data");
        for(int i=0;i<arrayfriends.length();i++)
        {
            JSONObject friend = arrayfriends.getJSONObject(i);
            JSONObject requestInfo = friend.getJSONObject("Friend");
            RequestItem requestItem = new RequestItem();
            requestItem.setId(requestInfo.getInt("id"));
            requestItem.setPersonId(requestInfo.getInt("person_id"));
            requestItem.setUserId(requestInfo.getInt("user_id"));
            requestItem.setIsGood(requestInfo.getBoolean("is_good"));
            User user = new User();
            JSONObject peopleInfo = friend.getJSONObject("People");
            user.setId(peopleInfo.getInt("id"));
            user.setName(peopleInfo.getString("name"));
            user.setEmail(peopleInfo.getString("email"));
            user.setPhone(peopleInfo.getString("phone"));
            requestItem.setUser(user);
            requestItems.add(requestItem);
        }
        return requestItems;
    }
}
